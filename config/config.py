import os
import re

class Config(object):
    DB_HOST="myhost"
    DB_NAME="mydb"
    DB_USER="myuser"
    DB_PASS="mypass"

    @classmethod
    def all(cls):
        regex = r'^[A-Z][A-Z_]*[A-Z]$'
        return [kv for kv in cls.__dict__.items() if re.match(regex, kv[0])]

if __name__=="__main__":
    for c in Config.all():
        print(c)
