import json
import pandas as pd
import numpy as np
from joblib import dump, load
from sklearn.pipeline import Pipeline, make_pipeline
from sklearn.preprocessing import FunctionTransformer
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score

# transformers
import transformers

# classification task
from sklearn.metrics import roc_auc_score, f1_score, roc_curve, accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
# add more ..

# regression task
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
# add more ..

# features from config
with open('../config/feature_sets.json') as json_file:
    feature_sets = json.load(json_file)



    

# train model
def train_model(df_train, target_name):
    
    target_name = str(target_name)

    # preprocessor with default and custom transformers 
    preprocessor = ColumnTransformer(
        transformers=[
            ('num',                 transformers.numeric_transformer,       feature_sets['numeric_features']),
            ('cat',                 transformers.categorical_transformer,   feature_sets['categorical_features'])
            # add custom transformers here ..
            # ('cus_1',                 transformers.MyTransformer,   feature_sets['MyTransformer_features'])
            ])

    # append model to preprocessing pipeline
    mod = Pipeline(steps=[('preprocessor', preprocessor),
                        ('classifier', LogisticRegression(solver='lbfgs'))
                         #('classifier', RandomForestClassifier()),
                         #('classifier', GradientBoostingClassifier()),
                         #('regressor', LinearRegression()),                       
                         #('regressor', RandomForestRegressor()),
                         #('regressor', GradientBoostingRegressor())
                         # add more ..
                         ])
    
    # separate target and features and fit model                
    x_train = df_train.drop(target_name, axis=1)
    y_train = df_train[target_name].fillna(0)
    mod.fit(x_train, y_train)

    # return model object
    return mod

# save model 
def save_model(mod,version):
    dump(mod, 'model_'+str(version)+'.joblib') 

# open saved model
def open_model(filename="my_model.joblib"):
    return load(filename)

if __name__=="__main__":
    for k,v in feature_sets.items():
        print(k)
        print(v)